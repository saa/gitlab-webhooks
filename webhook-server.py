#!/usr/bin/python

"""
A lightweight HTTPS server for use with GitLab webhooks

source: https://gitlab.uvm.edu/saa/gitlab-webhooks
author: Brian O'Donnell <brian.odonnell@uvm.edu>
"""

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import json, re, ssl

# Customize this string before use!!!
SECRET_KEY = 'SUPERSECRETALPHANUMERICSTRING'

# Port to bind the http server to
LISTEN_PORT = 8000

# Path to SSL certificate
CERT_FILE = './webhook-server.pem'

class WebhookHandler(BaseHTTPRequestHandler):
    """
    An inheritable class which implements a basic GitLab webhooks API
    """
    def exec_push(self, data):
        raise NotImplementedError('exec_push')

    def exec_tag_push(self, data):
        raise NotImplementedError('exec_tag_push')

    def exec_note(self, data):
        raise NotImplementedError('exec_note')

    def exec_issue(self, data):
        raise NotImplementedError('exec_issue')

    def exec_merge_request(self, data):
        raise NotImplementedError('exec_merge_request')

    def exec_build(self, data):
        raise NotImplementedError('exec_build')

    def respond(self, code, message):
        """
        Sends a customizable HTTP response
        """
        self.send_response(code)
        self.send_header("Content-type", "text/plain")
        self.send_header("Content-length", str(len(message)))
        self.end_headers()
        self.wfile.write(message)

    def do_POST(self):
        """
        Handles POST requests
        """
        try:
            # slurp in the request body
            self.rfile._sock.settimeout(5)
            content = self.rfile.read(int(self.headers['Content-Length']))
            postdata = json.loads(content)

            # parse out the 'secret key' from the url path
            m = re.match('\/([A-Za-z0-9]+)\/?$', self.path)

            if m:
                # Force the user to set a custom secret key
                if SECRET_KEY == 'SUPERSECRETALPHANUMERICSTRING' or SECRET_KEY == '' or SECRET_KEY is None:
                    raise ValueError('Please set a secret key!')

                if m.group(1) != SECRET_KEY:
                    self.respond(400, 'Invalid secret key')
                elif 'object_kind' in postdata:
                    action = postdata['object_kind']

                    if action == 'push':
                        self.exec_push(postdata)
                    elif action == 'tag_push':
                        self.exec_tag_push(postdata)
                    elif action == 'issue':
                        self.exec_issue(postdata)
                    elif action == 'note':
                        self.exec_note(postdata)
                    elif action == 'merge_request':
                        self.exec_merge_request(postdata)
                    elif action == 'build':
                        self.exec_build(postdata)
                    else:
                        self.respond(400, 'Invalid object_kind attribute')
                        return

                    self.respond(200, 'OK')
                else:
                    self.respond(400, 'Missing object_kind attribute')
            else:
                self.respond(400, 'Missing or invalid secret key in url')
        except NotImplementedError as e:
            self.respond(501, 'Method %s not implemented' % e)
        except (RuntimeError, ValueError) as e:
            self.respond(500, '%s' % e)

class CustomHookHandler(WebhookHandler):
    """
    Override parent class' exec_ methods here
    """
    pass

def main():
    """
    Starts the webserver
    """
    try:
        server = HTTPServer(('', LISTEN_PORT), CustomHookHandler)
        server.socket = ssl.wrap_socket(server.socket, certfile=CERT_FILE, server_side=True)
        server.serve_forever()
    except KeyboardInterrupt:
        print('Ctrl-C pressed, shutting down.')
    finally:
        server.socket.close()

if __name__ == '__main__':
    main()
